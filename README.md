# Email integration adapter

Using IMAP library, downloaded 100+ scheduled emailed report within 30mins of any file format to global location and converted all file format (like Excels, zip) into CSV along with current date. Using SMTP protocol, sent email for downloaded file status in success/fail format.

Reference:
1. https://docs.python.org/3.5/library/imaplib.html#imaplib.IMAP4
