from datetime import datetime
import os
now = datetime.now()
print(now.strftime("%Y-%m-%d"))
cur_date = now.strftime("%Y-%m-%d") 

def logMessage(message):    
    filename = 'mail_report_download_log.csv'
    current_run_filename = 'mail_report_download_log' + str(cur_date) + '.csv'    
    path = 'log/'
    msg = message
    task_name = ''
    end_time = ''
    try:
        os.stat(path+current_run_filename)
        # file exists
    except:
        if not os.path.exists(path):
            os.makedirs(path)
        File = open(path+current_run_filename, "w")  
    if os.stat(path+current_run_filename).st_size == 0:
        with open(path+current_run_filename, 'w') as csvFile:
            csvFile.write("date,mail_subject,file_type,task_name,start_time,end_time,status,error_message\n")
            csvFile.write(msg + "\n")
    else:
        with open(path+current_run_filename, 'a') as csvFile:
        	        csvFile.write(msg + "\n")
    with open(path+filename, 'a') as csvFile:
        csvFile.write(msg + "\n")
