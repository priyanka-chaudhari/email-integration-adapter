import email
import imaplib
import json
import datetime 
from datetime import date, timedelta
import os
import zipfile
import time
import fileinput
import re
import csv
import pandas as pd


today = date.today()
timestr = time.strftime("%Y-%m-%d")
start_date = date.today() - timedelta(30)

def searchMail(mail_session, subject, from1):
    # print(mail_session)
    try:        
        resp, items = mail_session.search(None,'SUBJECT','"'+subject+'"', 'FROM','"'+from1+'"')    
        print(resp)
        itemsidstr = items[0].split()
                
        # It converts the String array to numeric array        
        items = [int(numeric_string) for numeric_string in itemsidstr]        
        print("MAIL ID is:", items)

        if not items:
            mail_item_id = 0
        else:        
            
            date_array = []
            if resp == 'OK':
                # print(items)
                
                count_mail_id = 1
                # Traverse the emaild id in descending order so that first preference gets to the lastest mail 
                for emailid in sorted(items,reverse=True):                
                    mail_item_id = emailid
                    
                    resp, data = mail_session.fetch(str(emailid), "(BODY.PEEK[])")                    
                    email_body = data[0][1]                    
                    # print(email_body)
                    
                    mail = email.message_from_bytes(email_body)                                                         
                    # print('Raw Date:', mail['Delivery-date'])
                    mail_date = mail['Delivery-date']
                    
                    mail_date1 = email.utils.parsedate_tz(mail_date)
                    # print('mail_date1', mail_date1)                    
                    c_date = datetime.datetime.fromtimestamp(email.utils.mktime_tz(mail_date1))
                    # print("c_date",c_date)
                 
                    check_date = c_date.strftime('%Y-%m-%d')
                    # date_array.append(check_date)
                    # print('emailid',emailid)
                    
                    # print("check_date",check_date)
                    # if str(today) in date_array:
                    if str(start_date) <= str(check_date) <= str(today):
                        # print("Mail is present for date in range",check_date)
                        mail_item_id = str(emailid)
                        break                   
                    else:
                        # print("Mail not available for last date range")
                        mail_item_id = 0
                    
                    # Check if latest email_id assign if not 0 then break from FOR Loop
                    if mail_item_id != 0:
                        break
                    if count_mail_id == 4:
                        break
                    count_mail_id = count_mail_id + 1
    
    except Exception as err:
        
        print ("Exception occured in Search function:",Exception, err)
        error_msg = str(err)
        print("Re-run the process")
        import log_message as lm
        log_msg =  timestr + ',' + subject + ',,Search mail fail,,,Failed,' + error_msg
        print(log_msg)
        lm.logMessage(log_msg)  
        import imaplib_connect
        imaplib_connect.config_email_read()
        
    return mail_item_id

# Download all attachment files for a given email
# session, mailid, path, sourcefilenm, targetfilenm
def downloaAttachmentsInEmail(mail_session, email_id, path_dir, email_target_report_name, email_xlsx_sheet_name):
    try:
        
        resp, data = mail_session.fetch(str(email_id), "(BODY.PEEK[])")
        
        email_body = data[0][1]
        # mail = email.message_from_string(email_body)  # This wors fine for python 2.7        
        mail = email.message_from_bytes(email_body)

        if mail.get_content_maintype() != 'multipart':
            return "Attachement not found"
        for part in mail.walk():
            
            if part.get_content_maintype() != 'multipart' and part.get('Content-Disposition') is not None:
                file_name = part.get_filename()
                open(path_dir + '/' + part.get_filename(), 'wb').write(part.get_payload(decode=True))
                #get_file_extension = os.path.splitext(file_name)[1]
               
                #get_path_target_name = path_dir + '/' + file_name, path_dir + '/' + email_target_report_name + '_' + timestr + ".csv"
                if os.path.splitext(file_name)[1] == ".zip":
                    fantasy_zip = zipfile.ZipFile(path_dir + '/' + part.get_filename(), 'r')
                    getUnzipFileName = fantasy_zip.namelist()                
                    fantasy_zip.extractall(path_dir)
                    os.rename(path_dir + '/' + getUnzipFileName[0], path_dir + '/' + email_target_report_name + '_' + timestr + ".csv")
                    if file_name.endswith('.zip'):
                        os.unlink(path_dir + '/' +file_name)
                elif os.path.splitext(file_name)[1] == ".csv":
                    os.rename(path_dir + '/' + file_name, path_dir + '/' + email_target_report_name + '_' + timestr + ".csv")
                elif os.path.splitext(file_name)[1] == ".xlsx":
                    df = pd.read_excel(path_dir + '/' +file_name, sheet_name=int(email_xlsx_sheet_name), header=None, index=False)
                    df.to_csv(path_dir + '/' + email_target_report_name + '_' + timestr + ".csv", index=False)
                    if file_name.endswith('.xlsx'):
                        os.unlink(path_dir + '/' +file_name) 
                else:
                    os.rename(path_dir + '/' + file_name, path_dir + '/' + email_target_report_name + '_' + timestr + ".csv")
   
    except Exception as err:        
        print ("Exception in Download Attachment:", Exception, err)
        error_msg = str(err)
        print("Re-run the process")
        import log_message as lm
        log_msg =  timestr + ',,,Download fail,,,Failed,' + error_msg
        print(log_msg)
        lm.logMessage(log_msg)
        import imaplib_connect
        imaplib_connect.config_email_read()   
