import email
import imaplib
import json
import search_download
import log_message as lm
import datetime 
from datetime import date, timedelta
import time
from datetime import datetime
import time



class _Const(object):
    LOGIN_AUTH = 'email_config.json'
    EMAIL_DETAILS = 'mail_details_test.json'

class Login_email:
     # login Data Configuration JSON string
    loginDataConfigJSON = ''
    emailDataConfigJSON = ''

     # login Data Configuration Dictionary
    loginDataConfig = {}
    emailDataConfig = {}
    def __init__(self):
        try:
            CONST = _Const()
   
            with open(CONST.LOGIN_AUTH) as login_fileObject:
                self.loginDataConfig = json.load(login_fileObject)
                # print (self.loginDataConfig)

            with open(CONST.EMAIL_DETAILS) as email_fileObject:
                self.emailDataConfig = json.load(email_fileObject)

        except Exception as err:
            print (Exception,err)
            self.loginDataConfigJSON = ''
            self.loginDataConfig = {}
            self.emailDataConfig = {}

    #Returns the login data data dictionary.
    def getloginDataConfig(self):
        return self.loginDataConfig

    #Returns the email data dictionary.
    def getEmailDetailConfig(self):
        return self.emailDataConfig
    
def getLoginDetails(loginDataConfig):
    login_status = ''     
    for row in loginDataConfig['user_configuration']:
        # print(row['server'])
        
        imapSession = imaplib.IMAP4_SSL(row['server'])
        try:
            typ, accountDetails = imapSession.login(row['user_email'], row['user_password'])
            if typ != 'OK':
                print('Not able to sign in!')                
                login_status = 'False'
            else:
                print('Able to sign in!')                
                login_status = 'True'
            imapSession.select(row['mail_box']) # here you can choose a mail box like INBOX instead
        except imaplib.IMAP4.error as err:
            print("Login Failed", err)  
            login_status ='False' 
        except Exception as err:
            login_status = 'False'
            print("Login Failed", err)
        
    return imapSession,login_status

def email_config_read(getEmailConfig, mail_session):
    mail_notification_msg = ''
    
    date = (str(datetime.now().strftime("%y-%m-%d")))
    start_time = str(datetime.now())
    log_msg = ''
    mail_id = 0
    error_msg = ''     
    task_name = ''
    end_time = '' 
    status = ''
    path_dir = getEmailConfig['download_location_path']    
    for row in getEmailConfig['mail_details']:                                
        email_subject = str(row['subject'])
        email_from = str(row['from'])
        email_source_report_name = str(row['source_report_name'])
        email_target_report_name = str(row['target_report_name'])
        email_xlsx_sheet_name = str(row['sheet_name'])
        file_type = str(row['file_type'])
        try:
            if email_subject == '' or email_from == '':
                print("'Email subject' or 'Email from' has Blank value in mail_details.json")
                pass
            else:
                import search_download
                mail_id = search_download.searchMail(mail_session, email_subject, email_from)
                if mail_id != 0:                        
                    search_download.downloaAttachmentsInEmail(mail_session, mail_id, path_dir, email_target_report_name, email_xlsx_sheet_name)                   
                    end_time = str(datetime.now())   
                    task_name = 'Downloaded successfully'
                    status = 'Success'
                    error_msg = ''               
                else:
                    print("Mail Subject:'" + email_subject + "' not in mailbox or it might not be available for last 3 days")
                    task_name = 'Mail not available in Mail box'
                    status = 'No Mail available'
        
        except Exception as err:
            print("Exception in email_config_read function Imaplib_config")
            print (Exception, err)             
            status = 'Fail'
            
            task_name = 'Download Failed'
            error_msg = str(err)

            print("Re-run the process")
            config_email_read()                   
        
        log_msg =  date + ',' + email_subject + ',' + file_type + ','+ task_name +',' + start_time + ',' + end_time + ',' + status + ',' + error_msg
        notification_mail_msg = ''
        notification_mail_msg = status + '====' + error_msg + '====' + email_subject + '===='
        print(log_msg)
        lm.logMessage(log_msg)
        mail_notification_msg += notification_mail_msg + '\n' 
        # print(mail_notification_msg)
        # break
    print(mail_notification_msg)
    return mail_notification_msg

def config_email_read():
    loginDataConfig = {}
    emailDataConfig = {}
    
    login_email = Login_email()
    loginDataConfig = login_email.getloginDataConfig()
    emailDataConfig = login_email.getEmailDetailConfig()    

    # print(loginDataConfig)

    try:
        mail_session,login_status = getLoginDetails(loginDataConfig)
        if login_status == 'True':
            mail_notification_msg = email_config_read(emailDataConfig, mail_session)
            import mail_notification
            mail_notification.send_mail(loginDataConfig,mail_notification_msg)
        else:
            print("Login Failed check Credentials")

    except Exception as err:
        print("Exception in config_email", err)
        print("Re-run the process")
        config_email_read()

# config_email_read()
